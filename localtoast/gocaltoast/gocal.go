package main

import "gitlab.com/localtoast/gocaltoast/toast"

func main() {
	//call
	//Once more, with input
	rye := toast.Bake()
//	short := toast.Bake()
	raisin := toast.Bake()
	//My printfs has come!
//	toast.Print_Toast(rye)
//	toast.Print_Toast(short)
//	toast.Print_Toast(raisin)
	//Squish toast!
	loaf := toast.Squish_Toast(rye, raisin)
	toast.Print_Toast(true, loaf)
	
}
