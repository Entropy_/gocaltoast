package main

import (
	"fmt"

	"gitlab.com/localtoast/stringutil"
)

func main() {
	fmt.Printf("Hello, world.\n")
	fmt.Printf(stringutil.Reverse("Hello, World!"))
}
